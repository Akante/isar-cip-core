#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

SRC_URI[sha256sum] = "a83dcfe35b65485f9f433e0259dc3863ee5948ee425f15268069a60472d3c53d"
