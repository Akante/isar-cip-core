#
# CIP Core, generic profile
#
# Copyright (c) Siemens AG, 2023
#
# SPDX-License-Identifier: MIT

require linux-cip-common.inc

SRC_URI[sha256sum] = "8b578b83e7cd18c4649162d0c734e6af5c080f429f8636817ee402e195a4bdfb"
